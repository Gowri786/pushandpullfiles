package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class DeleteLead extends ProjectSpecificMethods{
	
	@Test
	public void deleting() throws InterruptedException
	{
		//Launching browser and till clicking on Find Leads link...
		/*startApp("chrome","http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, "crmsfa");
		WebElement elelogin = locateElement("classname","decorativeSubmit");
		click(elelogin);*/
		//doLogin();
		WebElement crm = locateElement("classname","crmsfa");
		click(crm);
		click(locateElement("xpath", "//a[@href='/crmsfa/control/leadsMain']"));
		click(locateElement("linktext", "Find Leads"));
		
		//clicking Phone tab...
		click(locateElement("xpath", "//*[text()='Phone']"));
		
		WebElement countryCode = locateElement("name", "phoneCountryCode");
		erase(countryCode);
		typeSpecial(countryCode, "91", "TAB");
		WebElement areaCode = locateElement("name", "phoneAreaCode");
		typeSpecial(areaCode, "605", "TAB");
		WebElement phoneNumber = locateElement("name", "phoneNumber");
		type(phoneNumber, "8754732388");
		
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(3000);
		//Getting text of and clicking on the first resulting lead id...
		String firstLeadId = getText(locateElement("xpath", "//*[@class='x-grid3-body']/div[1]/table[1]/tbody/tr[1]/td[1]"));
		click(locateElement("xpath", "//*[@class='x-grid3-body']/div[1]/table[1]/tbody/tr[1]/td[1]//a"));
		
		//Clicking on Delete button on the next form...
		click(locateElement("linktext", "Delete"));
		
		//Going back to Find Leads and verifying the deletion...
		click(locateElement("linktext", "Find Leads"));
		type(locateElement("name", "id"), firstLeadId);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		
		//String message = getText(locateElement("xpath", "//*[@class='x-paging-info']"));
		verifyExactText(locateElement("xpath", "//*[@class='x-paging-info']"), "No records to display");
		
		//closeBrowser();
	}

}
