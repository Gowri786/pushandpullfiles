package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;


public class CreateLead extends ProjectSpecificMethods {
	
		@Test
		public void login() throws InterruptedException{
			//Launching browser and till clicking Create Lead link steps....
			/*startApp("chrome","http://leaftaps.com/opentaps");
			WebElement eleUserName = locateElement("id", "username");
			type(eleUserName, "DemoSalesManager");
			WebElement elePassword = locateElement("id", "password");
			type(elePassword, "crmsfa");
			WebElement elelogin = locateElement("classname","decorativeSubmit");
			click(elelogin);*/
			//doLogin();
			WebElement crm = locateElement("classname","crmsfa");
			click(crm);
			WebElement cl = locateElement("linktext","Create Lead");
			click(cl);
			
			//working on Create Lead form
			WebElement comName = locateElement("createLeadForm_companyName");
			type(comName,"CTS");
			WebElement firstName = locateElement("createLeadForm_firstName");
			type(firstName,"Sweetline");
			//String firstNameCheck = getText(firstName);
			
			WebElement lastName = locateElement("createLeadForm_lastName");
			type(lastName,"Priya");
			WebElement firstNameLocal = locateElement("createLeadForm_firstNameLocal");
			type(firstNameLocal,"Gowri");
			WebElement lastNameLocal = locateElement("createLeadForm_lastNameLocal");
			type(lastNameLocal,"Karthik");
			WebElement personalTitle = locateElement("createLeadForm_personalTitle");
			type(personalTitle,"Mrs.");
			
			WebElement sourceDrop = locateElement("createLeadForm_dataSourceId");
			selectDropDown(sourceDrop, "visible", "Existing Customer");
						
			WebElement profTitle = locateElement("createLeadForm_generalProfTitle");
			type(profTitle,"Testing Lead");
			WebElement annRev = locateElement("createLeadForm_annualRevenue");
			type(annRev,"3k");
			
			WebElement industryDrop = locateElement("createLeadForm_industryEnumId");
			selectDropDown(industryDrop, "index", "3");
			
			WebElement ownershipDrop = locateElement("createLeadForm_ownershipEnumId");
			selectDropDown(ownershipDrop, "visible", "Sole Proprietorship");
			
			
			type(locateElement("createLeadForm_sicCode"),"SIC001");
			type(locateElement("createLeadForm_description"), "This is for testing...");
			type(locateElement("createLeadForm_importantNote"), "Attention pleaseeee...");
			erase(locateElement("createLeadForm_primaryPhoneCountryCode"));
			type(locateElement("createLeadForm_primaryPhoneCountryCode"), "91");
			type(locateElement("createLeadForm_primaryPhoneAreaCode"), "605");
			type(locateElement("createLeadForm_primaryPhoneExtension"), "32388");
			type(locateElement("createLeadForm_departmentName"), "Project Management");
			
			WebElement currencyDrop = locateElement("createLeadForm_currencyUomId");
			selectDropDown(currencyDrop, "index", "5");			
			
			type(locateElement("createLeadForm_numberEmployees"),"500");
			type(locateElement("createLeadForm_tickerSymbol"), "SASYA");
			type(locateElement("createLeadForm_primaryPhoneAskForName"), "Karthik");
			type(locateElement("createLeadForm_primaryWebUrl"), "https://sasya.com");
			type(locateElement("createLeadForm_generalToName"), "Gowri");
			type(locateElement("createLeadForm_generalAddress1"), "Lawspet");
			type(locateElement("createLeadForm_generalAddress2"), "Sivaji statue");
			type(locateElement("createLeadForm_generalCity"), "Puducherry");
			
			WebElement countryDrop = locateElement("createLeadForm_generalCountryGeoId");
			selectDropDown(countryDrop, "value", "IND");
			Thread.sleep(3000);
			WebElement stateDrop = locateElement("createLeadForm_generalStateProvinceGeoId");
			selectDropDown(stateDrop, "value", "IN-PY");
			
			type(locateElement("createLeadForm_generalPostalCode"), "605004");
			type(locateElement("createLeadForm_generalPostalCodeExt"), "004");
			
			WebElement marketingDrop = locateElement("createLeadForm_marketingCampaignId");
			selectDropDown(marketingDrop, "index", "8");
			
			type(locateElement("createLeadForm_primaryPhoneNumber"), "8754732388");
			type(locateElement("createLeadForm_primaryEmail"), "test@gmail.com");
			
			WebElement CLButton = locateElement("name", "submitButton");
			click(CLButton);
			
			WebElement firstNameAfterCreation = locateElement("viewLead_firstName_sp");
			verifyExactText(firstNameAfterCreation, "Sweetline");
			
			//closeBrowser();
			
		}
		@Test
		public void login1() throws InterruptedException{
			//Launching browser and till clicking Create Lead link steps....
			/*startApp("chrome","http://leaftaps.com/opentaps");
			WebElement eleUserName = locateElement("id", "username");
			type(eleUserName, "DemoSalesManager");
			WebElement elePassword = locateElement("id", "password");
			type(elePassword, "crmsfa");
			WebElement elelogin = locateElement("classname","decorativeSubmit");
			click(elelogin);*/
			//doLogin();
			WebElement crm = locateElement("classname","crmsfa");
			click(crm);
			WebElement cl = locateElement("linktext","Create Lead");
			click(cl);
			
			//working on Create Lead form
			WebElement comName = locateElement("createLeadForm_companyName");
			type(comName,"CTS");
			WebElement firstName = locateElement("createLeadForm_firstName");
			type(firstName,"Angeline");
			//String firstNameCheck = getText(firstName);
			
			WebElement lastName = locateElement("createLeadForm_lastName");
			type(lastName,"Gunavathy");
			WebElement firstNameLocal = locateElement("createLeadForm_firstNameLocal");
			type(firstNameLocal,"Gowri");
			WebElement lastNameLocal = locateElement("createLeadForm_lastNameLocal");
			type(lastNameLocal,"Karthik");
			WebElement personalTitle = locateElement("createLeadForm_personalTitle");
			type(personalTitle,"Mrs.");
			
			WebElement sourceDrop = locateElement("createLeadForm_dataSourceId");
			selectDropDown(sourceDrop, "visible", "Existing Customer");
						
			WebElement profTitle = locateElement("createLeadForm_generalProfTitle");
			type(profTitle,"Testing Lead");
			WebElement annRev = locateElement("createLeadForm_annualRevenue");
			type(annRev,"3k");
			
			WebElement industryDrop = locateElement("createLeadForm_industryEnumId");
			selectDropDown(industryDrop, "index", "3");
			
			WebElement ownershipDrop = locateElement("createLeadForm_ownershipEnumId");
			selectDropDown(ownershipDrop, "visible", "Sole Proprietorship");
			
			
			type(locateElement("createLeadForm_sicCode"),"SIC001");
			type(locateElement("createLeadForm_description"), "This is for testing...");
			type(locateElement("createLeadForm_importantNote"), "Attention pleaseeee...");
			erase(locateElement("createLeadForm_primaryPhoneCountryCode"));
			type(locateElement("createLeadForm_primaryPhoneCountryCode"), "91");
			type(locateElement("createLeadForm_primaryPhoneAreaCode"), "605");
			type(locateElement("createLeadForm_primaryPhoneExtension"), "32388");
			type(locateElement("createLeadForm_departmentName"), "Project Management");
			
			WebElement currencyDrop = locateElement("createLeadForm_currencyUomId");
			selectDropDown(currencyDrop, "index", "5");			
			
			type(locateElement("createLeadForm_numberEmployees"),"500");
			type(locateElement("createLeadForm_tickerSymbol"), "SASYA");
			type(locateElement("createLeadForm_primaryPhoneAskForName"), "Karthik");
			type(locateElement("createLeadForm_primaryWebUrl"), "https://sasya.com");
			type(locateElement("createLeadForm_generalToName"), "Gowri");
			type(locateElement("createLeadForm_generalAddress1"), "Lawspet");
			type(locateElement("createLeadForm_generalAddress2"), "Sivaji statue");
			type(locateElement("createLeadForm_generalCity"), "Puducherry");
			
			WebElement countryDrop = locateElement("createLeadForm_generalCountryGeoId");
			selectDropDown(countryDrop, "value", "IND");
			Thread.sleep(3000);
			WebElement stateDrop = locateElement("createLeadForm_generalStateProvinceGeoId");
			selectDropDown(stateDrop, "value", "IN-PY");
			
			type(locateElement("createLeadForm_generalPostalCode"), "605004");
			type(locateElement("createLeadForm_generalPostalCodeExt"), "004");
			
			WebElement marketingDrop = locateElement("createLeadForm_marketingCampaignId");
			selectDropDown(marketingDrop, "index", "8");
			
			type(locateElement("createLeadForm_primaryPhoneNumber"), "8754732388");
			type(locateElement("createLeadForm_primaryEmail"), "test@gmail.com");
			
			WebElement CLButton = locateElement("name", "submitButton");
			click(CLButton);
			
			WebElement firstNameAfterCreation = locateElement("viewLead_firstName_sp");
			verifyExactText(firstNameAfterCreation, "Sweetline");
			
			//closeBrowser();
		}
		
	}

